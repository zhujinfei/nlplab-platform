<%@page pageEncoding="UTF-8" %>
<html>
<head>
    <meta charset="utf-8">
    <title>文件上传</title>
</head>
<body>
<jsp:include page="layout/header.jsp"/>

<div class="container">
    <div class="container">
        <div class="page-header">
            <h2>文本文件上传</h2>
        </div>

        <p>文件上传要求：******</p>

        <p class="text-danger"></p>
        <p class="text-success"></p>


            <fieldset>
                <div class="form-group">
                    <label for="filename">上传文件：</label>
                    <input type="file" id="filename" name="files" class="form-control" multiple="multiple"/></div>
                <div class="form-group">
                    <label for="comments">文件描述：</label>
                    <textarea id="comments" name="comments" class="form-control"></textarea></div>
                <div class="form-group">
                    <input type="button" value="Send" class="btn btn-primary btn-large"/></div>
            </fieldset>
        <jsp:include page="layout/footer.jsp"/>

        <script type="text/javascript" src="/static/js/ajaxfileupload2.js"></script>
        <script type="text/javascript">
            $(function () {
                $(":button").click(function () {
                    ajaxFileUpload();
                })
            })
            function ajaxFileUpload() {
                $.ajaxFileUpload
                (
                        {
                            url: '/uploadfiles', //用于文件上传的服务器端请求地址
                            secureuri: false, //是否需要安全协议，一般设置为false
                            fileElementId: 'filename', //文件上传域的ID
                            dataType: 'json', //返回值类型 一般设置为json
                            success: function (data, status)  //服务器成功响应处理函数
                            {
                                alert("hello");
                                //alert(data);
                                var arr = JSON.parse(data);

                                if (arr.hasOwnProperty("errMsg")) {
                                    $(".text-success").text("");
                                    $(".text-danger").text(arr['errMsg']);
                                } else if (arr.hasOwnProperty("succMsg")) {
                                    $(".text-success").text(arr['succMsg'])
                                    $(".text-danger").text("")
                                }
                            },
                            error: function (data, status, e)//服务器响应失败处理函数
                            {
                                alert(e);
                            }
                        }
                )
                return false;
            }
        </script>
</body>
</html>