<%@page pageEncoding="UTF-8" %>
<html>
<head>
    <meta charset="utf-8">
    <title>文件上传</title>
</head>
<body>
<jsp:include page="layout/header.jsp"/>

<div class="container">
    <div class="container">
        <div class="page-header">
            <h2>文本文件上传</h2>
        </div>

        <p>文件上传要求：******</p>

        <p class="text-danger"></p>
        <p class="text-success"></p>


            <fieldset>
                <div class="form-group">
                    <input type="file" name="file_upload" id="file_upload" />
                <div class="form-group">
                    <label for="comments">文件描述：</label>
                    <textarea id="comments" name="comments" class="form-control"></textarea></div>

                    <input type="button" value="上传" class="btn btn-primary btn-large" onclick="$('#file_upload').uploadify('upload', '*')"/>
                    <input type="button" value="取消" class="btn btn-primary btn-large" onclick="$('#file_upload').uploadify('cancel', '*')"/>
            </fieldset>

        <jsp:include page="layout/footer.jsp"/>
        <link rel="stylesheet" type="text/css" href="/static/uploadify/uploadify.css"/>
        <script type="text/javascript" src="/static/uploadify/jquery.uploadify.min.js"></script>
        <script type="text/javascript">
            $(function() {
                $("#file_upload").uploadify({
                    'buttonText'      : '选择文件',
                    auto              : false,
                    height            : 30,
                    swf               : '/static/uploadify/uploadify.swf',
                    uploader          : '/uploadfiles1',
                    width             : 120,
                    'removeTimeout'   : 10,
                    'onUploadSuccess' : function(file, data, response) {
                        var obj = JSON.parse(data);
                        if(obj.Code == 0) {
                            $('#file_upload').uploadify('cancel', '*');
                            $(".text-success").text("");
                            $(".text-danger").text(obj.Msg);
                        }
                    },
                    'onQueueComplete' : function(queueData) {
                        alert(queueData.uploadsSuccessful + ' files were successfully uploaded.');
                    }
                });
            });
        </script>
</body>
</html>